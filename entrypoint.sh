#!/usr/bin/bash
set -eu

JENKINS_UID=$(id -u jenkins)
JENKINS_GID=$(id -g jenkins)

if [ "$(id -u)" != '0' ]; then
    PUID=$(id -u)
    PGID=$(id -g)
fi

if [ "$PUID" != "$JENKINS_UID" ]; then
    awk \
        -F ':' \
        -v PUID=$PUID '
        BEGIN           { OFS=FS }
        $3 == PUID      { sameId=$0; next }
        $1 == "jenkins" { $3=PUID; print; next }
                        { print }
        END             { if (sameId) print sameId }' /etc/passwd > /tmp/passwd

    cat /tmp/passwd > /etc/passwd
    rm /tmp/passwd
    sudo chown -R jenkins ~jenkins
fi

if [ "$PGID" != "$JENKINS_GID" ]; then
    sudo groupmod --gid $PGID --non-unique jenkins
    sudo chown -R :jenkins ~jenkins
fi

sudo chmod 644 /etc/passwd

if [ "$(id -u)" = '0' ]; then
    sudo -HE -u jenkins /usr/local/bin/jenkins-agent "$@"
else
    exec /usr/local/bin/jenkins-agent "$@"
fi
